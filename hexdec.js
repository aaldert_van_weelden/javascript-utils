function d2h(form)
  {
    var j=form.decimal.value;
    var hexchars =  "0123456789ABCDEF";
    var hv = "";
    for (var i=0; i< 4; i++)
      {
        k = j & 15;
        hv = hexchars.charAt(k) + hv;
        j = j >> 4;
      }
    form.hexadecimal.value = hv;  
}

function h2d(form)
{
  var j = form.hexadecimal.value.toUpperCase();
  var d = 0;
  var ch = ' ';
  var hexchars =  "0123456789ABCDEF";
  while (j.length < 4) j = 0 + j;
  for (var i = 0; i < 4; i++)
    {
      ch = j.charAt(i);
      d = d*16 + hexchars.indexOf(ch); 
    }
    form.decimal.value=d;
}


function d2b(form)
  { // Decimal to Binary
    j = form.decimal.value;
    for (i=0; i<16; ++i)
      {
        /* pick off the name of the box */
        l = eval("form.a"+i);
        /* set the value to 0 or 1 */
        if (j & 1)
          l.status=true;
        else
          l.status=false;
        j >>>= 1;
      }
  }

function b2d(form)
  { // Binary to Decimal
    j=0;
    for (i=15; i>=0; --i)
      {
        j <<= 1;
        l = eval("form.a"+i);
        if (l.status)
        j |= 1;
      }
    form.decimal.value=j;
  }
// -->

/**
 * The class to write logging to the server, queues messages and polls the server
 * @author Aaldert van Weelden
 * 
 * usage:
 * Use it as standalone object :  RemoteLogger.debug(TAG,line, message);
 * or use it after extending jQuery : $.extend(RemoteLogger);  $.debug(TAG,line,message);
 * 
 */
var RemoteLogger = (function(){
	
	var DEBUG 	= 'DEBUG',
		INFO 	= 'INFO',
		WARN 	= 'WARN',
		ERROR 	= 'ERROR',
		DUMP    = 'DUMP';
		
	
	//make xhr calls to the server
	var _xhr = {
			
			
			busy 		: false,
			queue   	: [],
			
			sendLog : function(tag,line,message,level){
				
				
				
				
				if(Util.getscriptUriParam('remotelogger')!=='true'){
					return;
				}
				
				if(typeof(APP_URL)==='undefined'){
					alert('The APP_URL is not defined, please add this to your configuration');
				}

				if(typeof(tag)==='undefined' || tag===null){
					tag = 'REMOTE_LOGGER';
				}
				if(tag!==null && Util.getExactType(tag)==='Number' && Util.getExactType(line)==='String' && (typeof(message)==='undefined' || message===null)){
					message = line;
					line = tag;
					tag = 'REMOTE_LOGGER';
				}
				
				
				
				if( (typeof(line)==='undefined' || line===null) && (typeof(message)==='undefined' || message===null)){
					message = tag;
					tag = 'REMOTE_LOGGER';
				}
				
				
				
				if(typeof(message)==='undefined' || message===null){
					message = '';
				}
				if(typeof(line)!=='undefined' && line!==null) {
					if(Util.getExactType(line)==='Number'){
						message = 'line '+line+'|'+message;
					}else{
						message = line;
					}
					
				}
				
				this.remoteLogger(message, level, tag);
				
			},
			
			remoteLogger : function(message,level,sender){
				
				if(typeof(API_KEY) === "undefined" || API_KEY === null){
					alert('API_KEY global variable is not defined, please add it to the project');
					return;
				}
				
				if(typeof(TOKEN) === "undefined" || TOKEN === null){
					alert('TOKEN global variable is not defined, please add it to the project');
					return;
				}
				
				var log = {
						message : message,
						level   : level,
						sender  : sender,
						token	: TOKEN,
						api_key	: API_KEY
					};
					
				//console.log('pushing message: '+message);
				
				this.queue.push(log);
				
				if(!this.busy){
					this.startTimer();
				}
				
				
			},
				
			//wait IMTERVAL ms for submission throttling
			startTimer : function(){
				var json='';
				var clone=[];
				_xhr.busy = true;
				
				setTimeout(function(){
					
					clone = _xhr.queue.slice(0);//clone queue array
					_xhr.queue=[];
					json=$.JSONtoString(clone);
					
					//send clone to server
					$.ajax({
						  type		: "POST",
						  url 		: APP_URL +'/api/logger/post',
						  data		: 'logs='+json
						})
						  .done(function( msg ) {
							  //console.log('SUCCESS');
							  _xhr.busy = false;
						  });
						
				},RemoteLogger.INTERVAL);
				
			}
					
					
					
					
				
							
	};
			
	
	var self = {
			/**
			 * logger submission throttling interval,adjust if neccessary
			 */
			INTERVAL : 750,
		
			logdebug : function(tag,line,message){
				_xhr.sendLog(tag,line,message,DEBUG);
			},
			
			/**
			 * dump a json string of an object, level is DEBUG.
			 * ### WARNING ### : do not use this for large objects like DOM related enriched ones, they will produce too much data
			 */
			logdump : function(tag,line,object){
				var message = $.JSONtoString(object);
				_xhr.sendLog(tag,line,message,DEBUG);
			},
			
			logwarn : function(tag,line,message){
				_xhr.sendLog(tag,line,message,WARN);
			},
			
			loginfo : function(tag,line,message){
				_xhr.sendLog(tag,line,message,INFO);
			},
			
			logerror : function(tag,line,message,error){
				if(error){
					message = message + ': ' + error
				}
				_xhr.sendLog(tag,line,message,ERROR);
			}	
			
	};
	
	return self;
	
	
})();

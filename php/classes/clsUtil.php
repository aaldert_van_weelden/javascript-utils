<?php
class Util {
	
	function getPath ($p_iIndex=0){
		
		$s_path='';
		$a_path=explode("/", $_SERVER['PHP_SELF']);
		
		if($p_iIndex>count($a_path)) {
			$p_iIndex=count($a_path);	
		}
		
		for($t=0;$t<=$p_iIndex;$t++) {
			$t<$p_iIndex?$slash='/':$slash='';
			$s_path .= $a_path[$t].$slash;
	
		}
		
		$path=$_SERVER['DOCUMENT_ROOT'].$s_path;
		$path=str_replace("//", "/", $path);
		return $path;
	}
	
	function getRoot ($p_iIndex=0){
		
		$s_path='';
		$a_path=explode("/", $_SERVER['PHP_SELF']);
		
		if($p_iIndex>count($a_path)) {
			$p_iIndex=count($a_path);	
		}
		
		for($t=0;$t<=$p_iIndex;$t++) {
			$t<$p_iIndex?$slash='/':$slash='';
			$s_path .= $a_path[$t].$slash;
	
		}
		
		$path=$_SERVER['HTTP_HOST'].$s_path;
		$path=str_replace("//", "/", $path);
		$path='http://'.$path;
		return $path;
	}
	//JSON
    function json_ready($data){
        
        $replace=array('\\t','\\n','\\',' ');
        $data=str_replace($replace,'',$data);
        $data=str_replace('"{','{',$data);
        $data=str_replace('}"','}',$data);
        return $data;
        
    }

	function uri2json($data,$b_JSON=false){
		$data=base64_decode($data);
		$data=urldecode($data);
		if(!$b_JSON){
			return $data;
		}
		else {
			return json_decode($data);
		}
		
	/*
	USE JAVASCRIPT UTIL FUNCTION
	safeURI 		: 
   * @description encodes uri-component to base64 decoded JSON-object for sending as url
   * @method safeURI
   * @public
   * @static
   * @param {string}  uri
   * @return {string} decoded object
	*/
	}




}
?>